import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { Foto } from '../../commons/Foto';

@Component({
  selector: 'page-home',
  templateUrl: 'verFoto.html'
})
export class ViewPhotoPage {

foto: Foto;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams
    ) {
      this.foto = this.navParams.get('id');
      
  }

}